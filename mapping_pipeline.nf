//bowtie2
nextflow.enable.dsl = 2
params.outdir = "./out/"
params.fastaIn = "./MK625182.fasta"
params.fastqIn = "./trim_SRR1777174.fastq"

process bowtie2 {
    publishDir "${params.outdir}", mode : "copy", overwrite: true
    container "https://depot.galaxyproject.org/singularity/bowtie2%3A2.4.4--py39hbb4e92a_0"

    input:
        path fastaIn
        path fastqIn
    output:
        path "${fastqIn.getSimpleName()}.sam"

    script:
    """
    bowtie2-build ${fastaIn} ./test
    bowtie2 -x test -U ${fastqIn} -p ${task.cpus} -S ${fastqIn.getSimpleName()}.sam
    """

}

/*
workflow bowtie {
    take:
    outdir 
    fastqIn 
    fastaIn

    main:
    bowtie2()
}
*/

workflow {
    fastaInChannel = channel.fromPath(params.fastaIn)
    fastqInChannel = channel.fromPath(params.fastqIn)
    bowtie2(fastaInChannel, fastqInChannel)
}

